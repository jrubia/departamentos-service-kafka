package profe.ms.empleadosweb.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import profe.empleados.model.Empleado;
import profe.ms.empleadosweb.exceptions.RestTemplateErrorHandler;

@Service
public class EmpleadosServiceStatic implements EmpleadosService {

	@Autowired
	private RestTemplateBuilder restTemplateBuilder;
	
	@Value("${app.empleadosServiceUrl}")
	private String empleadosServiceUrl;
	
	private RestTemplate getRestTemplate() {
		return restTemplateBuilder
				.errorHandler(new RestTemplateErrorHandler())
				.build();
	}
	
	@Override
	public Empleado[] getAllEmpleados() {
		return getRestTemplate().getForObject(empleadosServiceUrl, Empleado[].class);
	}

	@Override
	public Empleado getEmpleado(String cif) {
		return getRestTemplate().getForObject(empleadosServiceUrl + cif, Empleado.class);
	}

	@Override
	public void insertaEmpleado(Empleado emp) {
		getRestTemplate().postForLocation(empleadosServiceUrl, emp);
	}

	@Override
	public void modificaEmpleado(Empleado emp) {
		getRestTemplate().put(empleadosServiceUrl + emp.getCif(), emp);
	}

	@Override
	public void eliminaEmpleado(String cif) {
		getRestTemplate().delete(empleadosServiceUrl + cif);
	}

}
